import 'package:ej/models/car.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geocoder/geocoder.dart';

import '../models/wheels.dart';
import '../models/profile.dart';
import '../models/position.dart';

//Este metodo hace el llamado a la API geoCoder
Future<Position> getPositionAddres(String addres) async {
  if (addres.isEmpty) return null;
  try {
    final resp = await Geocoder.local.findAddressesFromQuery(addres);
    var a = resp?.first?.coordinates?.latitude;
    var b = resp?.first?.coordinates?.longitude;
    if (a != null && b != null) {
      return new Position(lat: a, lng: b);
    }
  } on PlatformException {
    print('addres not found');
  }
  return null;
}

Future<List<Wheels>> getWheels(
    WheelsDirecion wheelsDirecion, Profile profile) async {
  var p = getProfile('aa', 'aa');
  return <Wheels>[
    Wheels(
        cupos: 4,
        hora: TimeOfDay(hour: 5, minute: 3),
        profile: p,
        direction: wheelsDirecion),
    Wheels(
        cupos: 4,
        hora: TimeOfDay(hour: 5, minute: 3),
        profile: p,
        direction: wheelsDirecion),
    Wheels(
        cupos: 4,
        hora: TimeOfDay(hour: 5, minute: 3),
        profile: p,
        direction: wheelsDirecion),
    Wheels(
        cupos: 4,
        hora: TimeOfDay(hour: 5, minute: 3),
        profile: p,
        direction: wheelsDirecion),
    Wheels(
        cupos: 4,
        hora: TimeOfDay(hour: 5, minute: 3),
        profile: p,
        direction: wheelsDirecion),
    Wheels(
        cupos: 4,
        hora: TimeOfDay(hour: 5, minute: 3),
        profile: p,
        direction: wheelsDirecion),
    Wheels(
        cupos: 4,
        hora: TimeOfDay(hour: 5, minute: 3),
        profile: p,
        direction: wheelsDirecion),
    Wheels(
        cupos: 4,
        hora: TimeOfDay(hour: 5, minute: 3),
        profile: p,
        direction: wheelsDirecion),
    Wheels(
        cupos: 4,
        hora: TimeOfDay(hour: 5, minute: 3),
        profile: p,
        direction: wheelsDirecion),
    Wheels(
        cupos: 4,
        hora: TimeOfDay(hour: 5, minute: 3),
        profile: p,
        direction: wheelsDirecion),
  ];
}

Profile getProfile(String login, String password) {
  return Profile(
      nombre: 'Juanito',
      casa: Position(lat: 4.614649, lng: -74.068106),
      dirCasa: 'cll 28 #5b',
      dirUni: 'Universidad de los Andes',
      uni: Position(lat: 4.602449, lng: -74.064997),
      carros: getCarros(),
      celular: 31999999999,
      correo: 'jc.gloria@uniandes.edu.co',
      id: null,
      localidad: null,
      sexo: null);
}

List<Car> getCarros() {
  return <Car>[
    Car(marca: 'Tesla', modelo: 'Model S', placa: 'XYZ123', color: 'rojo'),
    Car(marca: 'Tesla', modelo: 'Model Y', placa: 'ABC987', color: 'verde')
  ];
}

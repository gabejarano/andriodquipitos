import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../views/createWheels.dart';

import '../views/confirmWheels.dart';

import '../models/profile.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../models/wheels.dart';
import '../bloc/dataAccess.dart';

class Home extends StatefulWidget {
  const Home({@required this.profile});
  final Profile profile;

  @override
  State<StatefulWidget> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final List<WheelsDirecion> dir = [
    WheelsDirecion.uni_casa,
    WheelsDirecion.casa_Uni
  ];

  int _selectedIndex = 0;
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil(width: 1080, height: 1920)..init(context);
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.profile.dirCasa,
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  letterSpacing: 2.0)),
          centerTitle: true,
          backgroundColor: Colors.black,
        ),
        body: FutureBuilder(
          future: getWheels(dir[_selectedIndex], widget.profile),
          builder: (conext, snapshot) {
            if (snapshot.hasData) {
              return _Contend(profile: widget.profile, wheels: snapshot.data);
            } else {
              return CircularProgressIndicator();
            }
          },
        ),
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Colors.grey[300],
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              title: Text('Ir a Casa'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.work),
              title: Text('Ir a Uniandes'),
            ),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: Colors.black,
          onTap: _onItemTapped,
        ),
        floatingActionButton: FloatingActionButton(
            backgroundColor: Colors.black,
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => CreateWheels(
                          wheelsDir: dir[_selectedIndex],
                          profile: widget.profile,
                        )),
              );
            },
            child: Text("+",
                style:
                    TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0))));
  }
}

class _Contend extends StatelessWidget {
  const _Contend({@required this.profile, @required this.wheels});
  final List<Wheels> wheels;
  final Profile profile;

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil(width: 1080, height: 1920)..init(context);
    return Column(children: <Widget>[
      Container(
        margin: EdgeInsets.symmetric(
            horizontal: ScreenUtil.getInstance().setWidth(10),
            vertical: ScreenUtil.getInstance().setWidth(10)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Icon(
              Icons.filter_list,
            ),
          ],
        ),
      ),
      Divider(height: 10),
      Expanded(
          child: ListView.builder(
        itemCount: wheels.length,
        itemBuilder: (BuildContext context, int index) {
          var a = wheels[index];
          return WheelView(
            wheels: a,
            profile: profile,
          );
        },
      ))
    ]);
  }
}

class WheelView extends StatefulWidget {
  const WheelView({@required this.wheels, @required this.profile});
  final Wheels wheels;
  final Profile profile;

  @override
  State<StatefulWidget> createState() {
    return StateWheelView();
  }
}

class StateWheelView extends State<WheelView> {
  bool siEs = false;

  String getHora() {
    var hour = widget.wheels?.hora;
    if (hour == null) return "NONE";
    return '${hour.hour != 12 ? hour.hour % 12 : 12}:${hour.minute < 10 ? '0' : ''}${hour.minute} ${(hour.hour < 12) ? 'am' : 'pm'}';
  }

  String toDir() {
    return (widget.wheels.direction == WheelsDirecion.casa_Uni)
        ? widget.wheels.profile.dirUni
        : widget.wheels.profile.dirCasa;
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil(width: 1080, height: 1920)..init(context);
    return GestureDetector(
        onTap: () async {
          final result = await Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ConfirmWheels(wheels: widget.wheels)),
          );
          setState(() {
            siEs = result;
          });
        },
        child: Container(
            decoration:
                BoxDecoration(color: (siEs ? Colors.amberAccent : Colors.transparent)),
            child: Card(
                color: Colors.white,
                elevation: 5,
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: ScreenUtil.getInstance().setWidth(25),
                      vertical: ScreenUtil.getInstance().setHeight(25)),
                  child: Column(children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          toDir(),
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        Text(
                          getHora(),
                          style: TextStyle(
                              fontSize:
                                  ScreenUtil(allowFontScaling: true).setSp(40)),
                        ),
                      ],
                    ),
                    Divider(height: ScreenUtil.getInstance().setHeight(12)),
                    Align(
                        alignment: Alignment.centerRight,
                        child: Container(
                            child: Text(
                          'Cupos: ${widget.wheels.cupos}',
                          style: TextStyle(
                              fontSize:
                                  ScreenUtil(allowFontScaling: true).setSp(40)),
                        )))
                  ]),
                ))));
  }
}

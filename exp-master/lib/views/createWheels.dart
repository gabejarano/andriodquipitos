import 'package:ej/views/createdWheels.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../models/position.dart';
import '../views/myMap.dart';

import '../models/profile.dart';
import '../models/wheels.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'home.dart';

class CreateWheels extends StatefulWidget {
  CreateWheels({@required this.wheelsDir, @required this.profile});
  final WheelsDirecion wheelsDir;
  final Profile profile;

  @override
  State<StatefulWidget> createState() {
    return StateCreateWheels();
  }
}

class StateCreateWheels extends State<CreateWheels> {
  TimeOfDay hour = TimeOfDay.now();
  TextEditingController _controller = TextEditingController();
  TextEditingController _controller2 = TextEditingController();

  String toDir() {
    return (widget.wheelsDir != WheelsDirecion.casa_Uni)
        ? "Ir a casa"
        : "Ir a la U";
  }

  Position toPos() {
    return (widget.wheelsDir != WheelsDirecion.casa_Uni)
        ? widget.profile.casa
        : widget.profile.uni;
  }

  String getHora() {
    if (hour == null) return "NONE";
    return '${hour.hour != 12 ? hour.hour % 12 : 12}:${hour.minute < 10 ? '0' : ''}${hour.minute} ${(hour.hour < 12) ? 'am' : 'pm'}';
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil(width: 1080, height: 1920)..init(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(toDir(),
            style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                letterSpacing: 2.0)),
        centerTitle: true,
        backgroundColor: Colors.black,
      ),
      body: Column(
        children: <Widget>[
          Expanded(child: MyMap(Key(toPos().toString()), positions: [toPos()])),
          Row(
            children: <Widget>[
              Text("Hora de salida: "),
              FlatButton(
                onPressed: () async {
                  TimeOfDay selectedTimeRTL = await showTimePicker(
                    context: context,
                    initialTime: TimeOfDay.now(),
                    builder: (BuildContext context, Widget child) {
                      return Directionality(
                        textDirection: TextDirection.rtl,
                        child: child,
                      );
                    },
                  );
                  setState(() {
                    hour = selectedTimeRTL;
                  });
                },
                child: Text(
                  getHora(),
                  style: TextStyle(
                    fontSize: ScreenUtil.getInstance().setSp(70),
                  ),
                ),
              )
            ],
          ),
          Row(
            children: <Widget>[
              Text("Cupos: "),
              Flexible(
                  child: Container(
                height: ScreenUtil.getInstance().setHeight(120),
                width: ScreenUtil.getInstance().setWidth(120),
                child: TextField(
                  controller: _controller,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                      hintText: '3',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5),
                      )),
                ),
              ))
            ],
          ),
          Row(
            children: <Widget>[
              Text("Punto de encuentro: "),
              Flexible(
                child: Container(
                  height: ScreenUtil.getInstance().setHeight(220),
                  width: ScreenUtil.getInstance().setWidth(800),
                  child: TextField(
                    maxLength: 1000,
                    maxLines: 6,
                    controller: _controller2,
                    decoration: InputDecoration(
                        hintText:
                            'nos vemos en la entrada del sd, tengo camisa azul',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                        )),
                  ),
                ),
              )
            ],
          ),
          Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                  margin: EdgeInsets.symmetric(
                      horizontal: ScreenUtil.getInstance().setWidth(40)),
                  child: FlatButton(
                    color: Colors.black,
                    child: Text(
                      "confirmar",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: ScreenUtil.getInstance().setSp(60)),
                    ),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => CreatedWheels(
                                  profile: widget.profile
                                )),
                      );
                    },
                  )))
        ],
      ),
    );
  }
}

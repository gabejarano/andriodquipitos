import 'package:ej/views/home.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../views/myMap.dart';

import '../models/wheels.dart';

import '../models/position.dart';
import 'package:flutter/material.dart';

class ConfirmWheels extends StatelessWidget {
  const ConfirmWheels({@required this.wheels});

  final Wheels wheels;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Wheels",
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  letterSpacing: 2.0)),
          centerTitle: true,
          backgroundColor: Colors.black,
        ),
        body: _Content(
          wheels: wheels,
        ));
  }
}

class _Content extends StatelessWidget {
  const _Content({@required this.wheels});
  final Wheels wheels;

  Position _finalPosition() {
    return (wheels.direction == WheelsDirecion.casa_Uni)
        ? wheels.profile.uni
        : wheels.profile.casa;
  }

  Position _initPosition() {
    return (wheels.direction == WheelsDirecion.casa_Uni)
        ? wheels.profile.casa
        : wheels.profile.uni;
  }

  String _finalDir() {
    return (wheels.direction == WheelsDirecion.casa_Uni)
        ? wheels.profile.dirUni
        : wheels.profile.dirCasa;
  }

  String getHora() {
    var hour = wheels?.hora;
    if (hour == null) return "NONE";
    return '${hour.hour != 12 ? hour.hour % 12 : 12}:${hour.minute < 10 ? '0' : ''}${hour.minute} ${(hour.hour < 12) ? 'am' : 'pm'}';
  }

//Se añaden las posiciones al array positions que se manda al mapa
  List<Position> getPositions() {
    List<Position> positions = [];
    positions.addAll([_finalPosition(), _initPosition()]);
    return positions;
  }

//esto es para crear la key para ponerse luego al mapa y que este se mueva
  String calculateKey(List<Position> positions) {
    String key = 'none';
    if (positions.isNotEmpty) {
      key = positions
          ?.reduce((acum, act) =>
              Position(lat: acum.lat + act.lat, lng: acum.lng + act.lng))
          ?.toString();
    }
    return key;
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil(width: 1080, height: 1920)..init(context);
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              _finalDir(),
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: ScreenUtil.getInstance().setSp(70)),
            ),
            Text(
              getHora(),
              style: TextStyle(fontSize: ScreenUtil.getInstance().setSp(40)),
            ),
          ],
        ),
        Divider(height: ScreenUtil.getInstance().setHeight(12)),
        Align(
            alignment: Alignment.centerRight,
            child: Container(
                child: Text(
              'Cupos: ${wheels.cupos}',
              style: TextStyle(fontSize: ScreenUtil.getInstance().setSp(50)),
            ))),
        Expanded(
            child: Stack(
          children: <Widget>[
            MyMap(Key(calculateKey(getPositions())), positions: getPositions()),
            Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                    margin: EdgeInsets.symmetric(horizontal: ScreenUtil.getInstance().setWidth(40)),
                    child: FlatButton(
                      color: Colors.black,
                      child: Text("confirmar", style: TextStyle(color: Colors.white, fontSize: ScreenUtil.getInstance().setSp(60)),),
                      onPressed: () {
                        Navigator.pop(context, true);
                      },
                    )))
          ],
        ))
      ],
    );
  }
}

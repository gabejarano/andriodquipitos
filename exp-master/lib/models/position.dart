import 'package:flutter/material.dart';

class Position {
  const Position({ this.lat, this.lng});
  final double lat;
  final double lng;

  @override
  String toString() {
    return '$lat-$lng';
  }

  Position.cargarJson(Map<String,dynamic> json) : 
  lat=json['lat'],
  lng=json['lng'];
}
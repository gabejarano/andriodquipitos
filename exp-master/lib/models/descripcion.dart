import 'package:flutter/cupertino.dart';

import 'car.dart';

class Descripcion {
  const Descripcion({this.carro,this.comentario});
  final Car carro;
  final String comentario;

  Descripcion.cargarJson(Map<String,dynamic> json) : 
  carro=json['carro'],
  comentario=json['comentario'];
}
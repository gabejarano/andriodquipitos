import '../models/profile.dart';
import 'package:flutter/material.dart';

import 'descripcion.dart';

enum WheelsDirecion { casa_Uni, uni_casa }

class Wheels {
  const Wheels(
      {this.cupos,
      this.hora,
      this.direction,
      this.profile,
      this.fechaCreacion,
      this.fechaPartida,
      this.fechaCuposCompletados,
      this.descripcion});

  final DateTime fechaCreacion;
  final DateTime fechaPartida;
  final DateTime fechaCuposCompletados;
  final int cupos;
  final TimeOfDay hora;
  final Profile profile;
  final WheelsDirecion direction;
  final Descripcion descripcion;

  Wheels.cargarJson(Map<String, dynamic> json)
      : fechaCreacion = json['fechaCreacion'],
        fechaPartida = json['fechaPartida'],
        fechaCuposCompletados = json['fechaCuposCompletados'],
        cupos = json['cupos'],
        hora = json['hora'],
        profile = json['profile'],
        direction = json['direction'],
        descripcion = json['descripcion'];
}

import 'package:flutter/material.dart';

import 'car.dart';
import 'position.dart';

class Profile {
  const Profile({
    this.nombre,
    this.casa,
    this.dirCasa,
    this.uni,
    this.dirUni,
    this.celular,
    this.carros,
    this.id,
    this.correo,
    this.sexo,
    this.fechaNacimiento,
    this.localidad,
  });
  final int id;
  final String nombre;
  final String correo;
  final Position casa;
  final String dirCasa;
  final Position uni;
  final int sexo;
  final DateTime fechaNacimiento;
  final String localidad;
  final String dirUni;
  final int celular;
  final List<Car> carros;

  Profile.cargarJson(Map<String, dynamic> json)
      : id = json['id'],
        nombre = json['nombre'],
        correo = json['correo'],
        casa = Position.cargarJson(json['casa']),
        dirCasa = json['dirCasa'],
        uni = Position.cargarJson(json['uni']),
        sexo = json['sexo'],
        fechaNacimiento = json['fechaNacimiento'],
        localidad = json['localidad'],
        dirUni = json['dirUni'],
        celular = json['celular'],
        carros = json['carros'].map((x) => Car.cargarJson(x));
}

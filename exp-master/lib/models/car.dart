import 'package:flutter/cupertino.dart';

class Car {
  const Car({this.placa,this.modelo,this.marca, this.color});
  final String placa;
  final String modelo;
  final String marca;
  final String color;

  Car.cargarJson(Map<String,dynamic> json) : 
  placa=json['placa'],
  modelo=json['modelo'],
  marca=json['marca'],
  color=json['color'];
}

import 'package:flutter/material.dart';

class MyTextField extends TextField {
  final Function onNonFocus;

  MyTextField(this.onNonFocus,{TextEditingController cont, Key key})
      : super(
          key: key,
          focusNode : FocusNode(),
          controller: cont
        ) {
    focusNode.addListener(() {
      if (!focusNode.hasFocus) {
          onNonFocus();
      }
    });
  }
}

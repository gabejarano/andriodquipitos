import 'package:flutter/material.dart';
import 'views/home.dart';
import 'bloc/dataAccess.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: Home(profile: getProfile('user', 'password')));
  }
}
